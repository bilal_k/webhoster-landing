import React from 'react'
import heroSVG from '../images/heroSVG.svg'
import '../css/Hero.css'

const Hero = () => {
    return (
        <>
            <div className='bg-[#0c8ce9] px-12 flex flex-col text-[#fdfcfa] xl:py-12 relative hero-main' id='home'>
                <h1 className='xl:text-5xl text-4xl font-bold xl:pt-12 xl:mt-8 mt-4 pt-8'>Need to host your website?</h1>
                <p className='xl:text-2xl pt-3 text-xl'>Webhoster has your back!</p>
                <p className='xl:text-2xl xl:pb-12 pb-3 text-xl'>We’re here to provide you with the best hosting services at the cheapest rate!</p>
                <button className='xl:text-3xl font-semibold bg-[#fdfcfa] text-[#0c8ce9] w-2/3 text-2xl rounded-3xl xl:w-1/6 xl:rounded-full p-3 border-2 transition-all border-[#fdfcfa] hover:bg-[#0c8ce9] hover:text-[#fdfcfa] mb-12'>Get started</button>
                <img src={heroSVG} className="absolute xl:-bottom-44 xl:right-28 -bottom-28 xl:w-1/2 w-5/6"/>
            </div>
        </>
    )
}

export default Hero