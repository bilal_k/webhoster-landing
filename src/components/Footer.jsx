import React from 'react'
import '../css/Footer.css'
import phoneIcon from '../images/phoneIcon.svg'
import twitterIcon from '../images/twitterIcon.svg'
import instaIcon from '../images/instaIcon.svg'
import linkedIcon from '../images/linkedIcon.svg'

const Footer = () => {
    return (
        <>
            <div className='bg-[#0c8ce9] footer-main pt-12 '>
                <div className='flex pb-5 xl:flex-row flex-col xl:items-start items-center xl:gap-0 gap-5'>
                    <div className=' w-3/4 text-center xl:text-start'>
                        <h1 className='text-[#fdfcfa] text-2xl font-semibold pb-2'>Webhoster</h1>
                        <p className='text-[#fdfcfa] text-lg w-2/2 xl:w-1/2'>We are a web hosting provider on a mission to bring success to everyone who goes online.
                            We do it by constantly improving server technology, providing professional support, and making the web hosting experience seamless.
                        </p>
                    </div>
                    <div className=' text-[#fdfcfa] text-xl flex flex-col gap-5 xl:w-1/2 w-2/2 justify-center'>
                        <p className='flex items-center xl:justify-start justify-center gap-4'><img src={instaIcon} className="w-[25px]"/>@webhosters</p>
                        <p className='flex items-center xl:justify-start justify-center gap-4'><img src={linkedIcon} className="w-[25px]"/>linkedin.com/in/webhoster</p>
                        <p className='flex items-center xl:justify-start justify-center gap-4'><img src={twitterIcon} className="w-[25px]"/>@webhosters</p>
                        <p className='flex items-center xl:justify-start justify-center gap-4 '><img src={phoneIcon} className="w-[25px]"/>+92 111244622</p>
                    </div>
                </div>
                <div className='xl:text-md text-sm text-[#fdfcfa] flex justify-between items-center border-t pt-3 mt-5 pb-2 xl:gap-0 gap-7'>
                    <p className=''>© 2022-2022 webhoster.com | Providing web hosting and cloud services</p>
                    <p>All rights reserved.</p>
                </div>
            </div>
        </>
    )
}

export default Footer