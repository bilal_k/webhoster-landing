import React, { useEffect } from 'react'
import '../css/Header.css'

const Header = () => {

    const scroll2Element = (elemID) => {
        window.scrollTo({
            top: document.getElementById(elemID).offsetTop - 120,
            behavior: "smooth",
        });
    }

    const linkHandler = (e) => {
        e.preventDefault();

        const goto = e.target.getAttribute("goto");
        setTimeout(() => {
            scroll2Element(goto);

        }, 0);
    }

    useEffect(() => {
        const navbar = document.getElementById('navContainer');
        const logo = document.getElementById('logo');
        const items = document.getElementsByClassName('navbarItem');
        const btn = document.getElementById('navbarBtn');
        const sticky = navbar.offsetTop;

        const navBarFix = () => {
            if (window.pageYOffset > sticky) {
                navbar.classList.add('sticky');
                navbar.style.backgroundColor = "#fcfdfa";
                logo.style.color = "#0c8ce9"
                btn.style.borderColor = "#0c8ce9";
                btn.style.color = "#0c8ce9";
                for (let i = 0; i < items.length; i++) {
                    items[i].style.color = "#0c8ce9"
                }
                btn.onmouseover = () => {
                    btn.style.color = "#fdfcfa"
                    btn.style.backgroundColor = "#0c8ce9"
                    console.log('hovered')
                }
                btn.onmouseleave = () => {
                    btn.style.color = "#0c8ce9"
                    btn.style.backgroundColor = "#fdfcfa"
                }


            }
            else {
                btn.style.borderColor = "#fdfcfa";
                btn.style.backgroundColor = "transparent"
                btn.style.color = "#fdfcfa";
                btn.onmouseover = () => {
                    btn.style.color = "#0c8ce9"
                    btn.style.backgroundColor = "#fdfcfa"
                    console.log('hovered')
                }
                btn.onmouseleave = () => {
                    btn.style.color = "#fdfcfa"
                    btn.style.backgroundColor = "transparent"
                }
                navbar.classList.remove('sticky');
                navbar.style.backgroundColor = "#0c8ce9"
                for (let i = 0; i < items.length; i++) {
                    items[i].style.color = "#fdfcfa"
                }
                logo.style.color = "#fdfcfa"
            }
        }

        window.onscroll = function () {
            navBarFix();
        }

    });


    return (

        <>
            <div className='bg-[#0C8CE9] p-3 py-5 flex items-center header-main sticky top-0 z-10 ' id='navContainer'>
                <h1 className='text-3xl font-bold text-[#FDFCFA] trackin w-1/2 cursor-pointer' id="logo" onClick={linkHandler} goto="home">Webhoster</h1>
                <div className='w-1/2 flex gap-9 text-xl text-[#fdfcfa] items-center justify-end cursor-pointer'>
                    <p className='navbarItem' onClick={linkHandler} goto="home">Home</p>
                    <p className='navbarItem' onClick={linkHandler} goto="about">About</p>
                    <p className='navbarItem' onClick={linkHandler} goto="contact">Contact</p>
                    <button id="navbarBtn" className='rounded-3xl p-2 px-5 border-2 transition-all font-medium'>Host today</button>
                </div>
            </div>
        </>

    )
}

export default Header