import React from 'react'
import '../css/Contact.css'

const Contact = () => {
    return (
        <>
            <div className='border-2 rounded-3xl border-[#0c8ce9] xl:w-4/6 w-2/2 mx-5 p-6 xl:mx-auto contact-main mb-24 pb-10' id='contact'>
                <h1 className='text-3xl text-[#0c8ce9] font-bold'>Contact us</h1>
                <div className='pt-7 flex flex-col gap-5'>
                    <div className='flex gap-7'>
                        <input type="text" className='border-2 border-[#0c8ce9] rounded-xl p-3 pl-5 outline-none text-[#0c8ce9] placeholder-[#7AC6FE] w-1/2 text-xl' placeholder='First name' />
                        <input type="text" className='border-2 border-[#0c8ce9] rounded-xl p-3 pl-5 outline-none text-[#0c8ce9] placeholder-[#7AC6FE] w-1/2 text-xl' placeholder='Last name' />
                    </div>
                    <input type="email" className='border-2 border-[#0c8ce9] rounded-xl p-3 pl-5 outline-none text-[#0c8ce9] placeholder-[#7AC6FE] text-xl' placeholder='Email' />
                    <textarea className='border-2 border-[#0c8ce9] rounded-xl p-3 pl-5 outline-none text-[#0c8ce9] placeholder-[#7AC6FE] text-xl resize-none overflow-auto' rows={8} placeholder='Send a message' />
                </div>
                <div className='flex justify-end'>
                    <button className='bg-[#0c8ce9] p-3 px-10 text-2xl rounded-3xl text-[#fdfcfa] font-semibold mt-8 transition-all hover:bg-[#0d84d8]'>Submit</button>
                </div>
            </div>
        </>

    )
}

export default Contact