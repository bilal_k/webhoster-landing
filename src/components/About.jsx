import React from 'react'
import heartIcon from '../images/heartIcon.svg'
import dollarIcon from '../images/dollarIcon.svg'
import meterIcon from '../images/meterIcon.svg'

const About = () => {
    return (
        <>
            <div className=' py-12 xl:my-12 text-center flex flex-col items-center gap-8 w-5/6 justify-center mx-auto ' id='about'>
                <h1 className='text-[#0c8ce9] text-4xl font-bold pt-12 mt-12'>Who are we?</h1>
                <p className='text-2xl text-[#0c8ce9] xl:w-3/4 pb-5 w-2/2'>
                    We at Webhoster are here to provide you with the best
                    hosting services for your website.
                    You can trust us with hosting our website on the cloud
                    without unnecessary delays and at amazing prices!
                </p>
                <div className='flex gap-10 pt-5 xl:flex-row flex-col'>
                    <div className='flex flex-col items-center justify-center'>
                        <img src={dollarIcon} className="" />
                        <p className='text-3xl font-medium text-[#0c8ce9] pt-5'>Cheap.</p>
                        <p className='text-xl text-[#0c8ce9]'>We provide the cheapest rates for hosting your website!</p>
                    </div>
                    <div className='flex flex-col items-center'>
                        <img src={meterIcon} className="" />
                        <p className='text-3xl font-medium text-[#0c8ce9] pt-5'>Fast.</p>
                        <p className='text-xl text-[#0c8ce9]'>With our services,
                            your website will reach
                            its maximum potential!</p>
                    </div>
                    <div className='flex flex-col items-center'>
                        <img src={heartIcon} className="" />
                        <p className='text-3xl font-medium text-[#0c8ce9] pt-5'>Reliable.</p>
                        <p className='text-xl text-[#0c8ce9]'>Expect your website
                            to never be down and
                            always a click away!</p>
                    </div>
                </div>
            </div>
        </>

    )
}

export default About